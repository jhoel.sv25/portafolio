import { Service } from '@interfaces/service.interface'

export const SERVICES: Service[] = [
  {
    icon: 'globe',
    title: 'Paginas web',
    description: 'Creación de sitios web responsives, atractivos y funcionales. para tu negocio.'
  },
  {
    icon: 'dev',
    title: 'Desarrollo de aplicaciones web',
    description: 'Desarrollo de aplicaciones web a medida, adaptadas a sus necesidades.'
  },
  {
    icon: 'seo',
    title: 'SEO',
    description:
      'Optimización de su sitio web para mejorar su posicionamiento en los motores de búsqueda.'
  }
]

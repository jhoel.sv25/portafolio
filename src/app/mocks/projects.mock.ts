import { Project } from '@interfaces/projects.interface'

export const PROJECTS: Project[] = [
  {
    id: 1,
    title: 'Sitio web de consulta con correo electrónico',
    description:
      'Sitio web de consulta con correo electrónico para. se puede manejar envio de correo desde el mismo sitio web.  ',
    image: 'assets/img/01/01.webp',
    technologies: ['Angular', 'TypeScript', 'NestJS', 'Tailwindcss'],
    url: 'https://website-eta-kohl-46.vercel.app/',
    github: '',
    date: '2021-01-01',
    tumbails: ['assets/img/01/02.webp', 'assets/img/01/03.webp', 'assets/img/01/04.webp']
  },
  {
    id: 2,
    title: 'Carrito de compras  de productos ',
    description:
      ' Una aplicación de carrito de compras de comercio electrónico. Este proyecto proporciona una experiencia de compra perfecta con funciones como agregar y eliminar artículos del carrito. ',
    image: 'assets/img/02/01.webp',
    technologies: ['Angular', 'TypeScript', 'Taiwlindcss'],
    url: '',
    github: 'https://github.com/jhoelsv25/shooping',
    date: '2021-01-01',
    tumbails: []
  },
  {
    id: 3,
    title: 'Plantilla para ecommerce',
    description:
      'La plantilla proporciona una interfaz fácil de usar para buscar y comprar productos. Este proyecto garantiza una base de código modular y eficiente. Los usuarios pueden explorar una variedad de productos, ',
    image: 'assets/img/03/01.webp',
    technologies: ['React', 'JavaScript', 'Tailwindcss'],
    url: '',
    github: 'https://github.com/jhoelsv25/proyects-react/tree/main/proyects/shopping-cart',
    date: '2021-01-01',
    tumbails: []
  },
  {
    id: 4,
    title: 'Plantilla - Inicio de sesion',
    description:
      'Plantilla de inicio de sesión con un diseño simple y elegante. Este proyecto proporciona una interfaz de usuario intuitiva y fácil de usar. ',
    image: 'assets/img/04/01.webp',
    technologies: ['Angular', 'TypeScript', 'Tailwindcss'],
    url: '',
    github: 'https://github.com/jhoelsv25/login-new',
    date: '2021-01-01',
    tumbails: []
  },
  {
    id: 5,
    title: 'Sistema de Gestion de Hotel',
    description:
      'Sistema de gestión de hotel con funciones como reservas, gestión de habitaciones y facturación. Este proyecto proporciona una interfaz de usuario fácil de usar y una base de código modular. ',
    image: 'assets/img/05/01.png',
    technologies: ['Angular', 'TypeScript', 'Tailwindcss', 'NestJS', 'TypeORM'],
    url: 'https://hotel-web-tan.vercel.app/auth/login',
    github: '',
    date: '2024-07-01',
    tumbails: ['assets/img/05/02.png', 'assets/img/05/03.png', 'assets/img/05/04.png']
  }
]

import { Experience } from '@interfaces/experience.interface'

export const EXPERIENCE: Experience[] = [
  {
    title: 'Fullstack Developer',
    company: 'Freelancer',
    description:
      'Creacion de aplicaciones web a medida, adaptadas a las necesidades del cliente, con la tecnologia mas adecuada para el proyecto.',
    date: 'Actualmente',
    technologies: ['Tailwindcss', 'Angular', 'NestJS', 'MongoDB', 'Postgres']
  },
  {
    title: 'Frontend Developer',
    company: 'Jhuno SAC',
    description:
      'Desarrollor de aplicaciones web. Creación de sitios web responsives, atractivos y funcionales.',
    date: 'Julio 2023',
    technologies: ['HTML', 'CSS', 'JavaScript', 'Angular', 'Python', 'Odoo']
  }
]

export interface Project {
  id: number
  title: string
  description: string
  image: string
  technologies: string[]
  url: string
  github: string
  date: string
  tumbails: string[]
}

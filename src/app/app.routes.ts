import { Routes } from '@angular/router'

export const routes: Routes = [
  {
    path: '',
    loadComponent: () => import('./pages/home/home.component'),
    children: [
      {
        path: '',
        loadComponent: () => import('./pages/projects/projects.component')
      },
      {
        path: 'project/:id',
        loadComponent: () => import('./pages/project-detail/project-detail.component')
      }
    ]
  }
]

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component } from '@angular/core'
import { RouterLink } from '@angular/router'

@Component({
  selector: 'app-header',
  standalone: true,
  imports: [CommonModule, RouterLink],
  templateUrl: './header.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styles: [
    `
      nav {
        animation: nav-shadown 1s linear both;
        animation-timeline: scroll();
        animation-range: 0 100px;
      }

      @keyframes nav-shadown {
        to {
          @apply bg-gray-400/50 text-gray-900 shadow-lg ring-1 ring-white/10 backdrop-blur dark:bg-gray-800/90 dark:text-gray-100;
        }
      }
    `
  ]
})
export class HeaderComponent {
  public links = [
    { name: 'Experiencia', label: 'experiencia', path: '#experiencia' },
    { name: 'Proyectos', label: 'proyectos', path: '#proyectos' },
    { name: 'Servicios', label: 'servicios', path: '#servicios' },
    { name: 'Sobre mi', label: 'sobre-mi', path: '#sobremi' },
    { name: 'Contacto', label: 'contacto', path: 'mailto:jhoel.sv@gmail.com' }
  ]
}

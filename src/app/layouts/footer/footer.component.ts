import { ChangeDetectionStrategy, Component } from '@angular/core'
import { GithubIcon } from '@components/icons/github.component'
import { ThemeComponent } from '@components/theme/theme.component'

@Component({
  selector: 'app-footer',
  standalone: true,
  imports: [ThemeComponent, GithubIcon],
  templateUrl: './footer.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterComponent {
  public currentYear = new Date().getFullYear()
}

import { Component, input } from '@angular/core'

@Component({
  selector: 'btn-social',
  standalone: true,
  template: `
    <a
      [href]="href()"
      target="_blank"
      rel="noopener noreferrer"
      role="link"
      class=" group inline-flex w-40 max-w-fit items-center justify-center gap-2 rounded-full border border-gray-300 bg-gray-100 px-4 py-1 text-base text-gray-800 transition-all duration-300 hover:scale-105  hover:border-gray-700 hover:bg-gray-900 hover:text-white focus:outline-none focus-visible:outline-none focus-visible:ring focus-visible:ring-lima-500/80 focus-visible:ring-white focus-visible:ring-offset-2 active:bg-black dark:border-gray-600 dark:bg-gray-800 dark:text-white"
    >
      <ng-content />
    </a>
  `
})
export class BtnSocialComponent {
  public href = input.required<string>()
}

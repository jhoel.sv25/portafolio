import { ChangeDetectionStrategy, Component, input } from '@angular/core'
import { RouterLink, RouterLinkWithHref } from '@angular/router'
import { Project } from '@interfaces/projects.interface'

@Component({
  selector: 'card-projets',
  standalone: true,
  imports: [RouterLink],
  templateUrl: './card-projets.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardProjetsComponent {
  public project = input.required<Project>()
}

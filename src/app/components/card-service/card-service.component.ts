import { ChangeDetectionStrategy, Component, input } from '@angular/core'
import { DevIcon } from '@components/icons/dev.component'
import { GolbeIcon } from '@components/icons/global.component'
import { SeoIcon } from '@components/icons/seo.component'
import { Service } from '@interfaces/service.interface'

@Component({
  selector: 'card-service',
  standalone: true,
  imports: [GolbeIcon, DevIcon, SeoIcon],
  templateUrl: './card-service.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardServiceComponent {
  public service = input.required<Service>()
}

import { ChangeDetectionStrategy, Component, input } from '@angular/core'
import { Experience } from '@interfaces/experience.interface'

@Component({
  selector: 'card-experience',
  standalone: true,
  templateUrl: './card-experience.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardExperienceComponent {
  public experience = input.required<Experience>()
}

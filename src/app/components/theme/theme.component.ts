import { NgClass } from '@angular/common'
import { Component, OnInit, signal } from '@angular/core'
import { DarkIcon } from '@components/icons/dark.component'
import { LightIcon } from '@components/icons/light.component'
import { SystemIcon } from '@components/icons/system.component'

@Component({
  selector: 'app-theme',
  standalone: true,
  imports: [NgClass, DarkIcon, SystemIcon, LightIcon],
  template: `
    <div class="flex gap-3">
      <button
        class="text-gray-800 hover:scale-110 hover:text-lime-500 dark:text-gray-100"
        [ngClass]="{ 'text-lima-400 dark:text-lima-400': theme() === 'dark' }"
        (click)="toggleTheme('dark')"
      >
        <light-icon />
      </button>
      <button
        class="text-gray-800 hover:scale-110 hover:text-lime-500 dark:text-gray-100"
        [ngClass]="{ 'text-lima-400 dark:text-lima-400': theme() === 'light' }"
        (click)="toggleTheme('light')"
      >
        <dark-icon />
      </button>

      <button
        class="text-gray-800 hover:scale-110 hover:text-lime-500 dark:text-gray-100"
        [ngClass]="{ 'text-lima-400 dark:text-lima-400': theme() === 'system' }"
        (click)="toggleTheme('system')"
      >
        <system-icon />
      </button>
    </div>
  `
})
export class ThemeComponent implements OnInit {
  public theme = signal<string>('system')
  public isDarkMode = signal<boolean>(false)

  ngOnInit(): void {
    if (localStorage.getItem('theme')) {
      this.theme.set(localStorage.getItem('theme') as string)
      this.toggleTheme(this.theme())
    } else {
      window.matchMedia('(prefers-color-scheme: dark)').matches
        ? document.documentElement.classList.add('dark')
        : document.documentElement.classList.remove('dark')
    }
  }

  toggleTheme(theme: string) {
    this.theme.set(theme)
    this.setLocalStorageTheme(this.theme())
    switch (theme) {
      case 'light':
        document.documentElement.classList.remove('dark')
        break
      case 'dark':
        document.documentElement.classList.add('dark')
        break
      case 'system':
        window.matchMedia('(prefers-color-scheme: dark)').matches
          ? document.documentElement.classList.add('dark')
          : document.documentElement.classList.remove('dark')
        break
    }
  }
  private setLocalStorageTheme(theme: string) {
    localStorage.setItem('theme', theme)
  }
}

import { ChangeDetectionStrategy, Component, input } from '@angular/core'

@Component({
  selector: 'badge',
  standalone: true,
  template: ` <div class="relative flex items-center rounded-full bg-lima-500 px-3 py-1 text-sm">
    <ng-content />
  </div>`,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BadgeComponent {}

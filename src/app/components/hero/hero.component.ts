import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, input } from '@angular/core'
import { BadgeComponent } from '@components/badge/badge.component'
import { BtnSocialComponent } from '@components/btn-social/btn-social.component'
import { EmailIcon } from '@components/icons/email.component'
import { LinkedinIcon } from '@components/icons/linkedin.component'

@Component({
  selector: 'hero',
  standalone: true,
  imports: [CommonModule, BadgeComponent, BtnSocialComponent, EmailIcon, LinkedinIcon],
  templateUrl: './hero.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeroComponent {}

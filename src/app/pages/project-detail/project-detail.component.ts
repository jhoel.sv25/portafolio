import { CommonModule, NgClass } from '@angular/common'
import { ChangeDetectionStrategy, Component, inject, OnInit, signal } from '@angular/core'
import { ActivatedRoute, RouterLinkWithHref } from '@angular/router'
import { GithubIcon } from '@components/icons/github.component'
import { LinkIcon } from '@components/icons/link.component'
import { Project } from '@interfaces/projects.interface'

@Component({
  selector: 'app-project-detail',
  standalone: true,
  imports: [RouterLinkWithHref, NgClass, GithubIcon, LinkIcon],
  templateUrl: './project-detail.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export default class ProjectDetailComponent implements OnInit {
  private route = inject(ActivatedRoute)
  public project = signal<Project | null>(null)
  public previewImage = signal<string | null>(null)

  ngOnInit(): void {
    this.route.paramMap.subscribe(() => {
      const data = history.state.project as Project
      if (data) {
        this.previewImage.set(data.image)
        this.project.set(data)
      }
    })
  }

  public previewImg(image: string): void {
    this.previewImage.set(image)
  }
}

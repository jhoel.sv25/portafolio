import { ChangeDetectionStrategy, Component, signal } from '@angular/core'
import { BtnSocialComponent } from '@components/btn-social/btn-social.component'
import { CardServiceComponent } from '@components/card-service/card-service.component'
import { EmailIcon } from '@components/icons/email.component'
import { ServiceIcon } from '@components/icons/service.component'
import { Service } from '@interfaces/service.interface'
import { SERVICES } from '@mocks/services.mock'

@Component({
  selector: 'app-services',
  standalone: true,
  imports: [CardServiceComponent, BtnSocialComponent, ServiceIcon, EmailIcon],
  templateUrl: './services.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ServicesComponent {
  public services = signal<Service[]>(SERVICES)
}

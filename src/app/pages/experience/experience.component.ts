import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, signal } from '@angular/core'
import { CardExperienceComponent } from '@components/card-experience/card-experience.component'
import { ExperienceIcon } from '@components/icons/experience.component'
import { Experience } from '@interfaces/experience.interface'
import { EXPERIENCE } from '@mocks/experience.mock'

@Component({
  selector: 'app-experience',
  standalone: true,
  imports: [CardExperienceComponent, ExperienceIcon],
  templateUrl: './experience.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExperienceComponent {
  public experiences = signal<Experience[]>(EXPERIENCE)
}

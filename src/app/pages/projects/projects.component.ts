import { ChangeDetectionStrategy, Component, signal } from '@angular/core'
import { RouterOutlet } from '@angular/router'
import { CardProjetsComponent } from '@components/card-projets/card-projets.component'
import { Project } from '@interfaces/projects.interface'
import { PROJECTS } from '@mocks/projects.mock'

@Component({
  selector: 'app-projects',
  standalone: true,
  imports: [RouterOutlet, CardProjetsComponent],
  templateUrl: './projects.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export default class ProjectsComponent {
  public projects = signal<Project[]>(PROJECTS)
}

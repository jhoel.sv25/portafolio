import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component } from '@angular/core'
import { EmailIcon } from '@components/icons/email.component'
import { GithubIcon } from '@components/icons/github.component'
import { InstagramIcon } from '@components/icons/instagram.component'
import { LinkedinIcon } from '@components/icons/linkedin.component'
import { UserIcon } from '@components/icons/user.component'

@Component({
  selector: 'app-about-me',
  standalone: true,
  imports: [EmailIcon, LinkedinIcon, GithubIcon, UserIcon, InstagramIcon],
  templateUrl: './about-me.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AboutMeComponent {}

import { CommonModule } from '@angular/common'
import { ChangeDetectionStrategy, Component, computed, inject } from '@angular/core'
import { RouterOutlet } from '@angular/router'
import { HeroComponent } from '@components/hero/hero.component'
import { CodeIcon } from '@components/icons/code.component'
import { FooterComponent } from '@layouts/footer/footer.component'
import { HeaderComponent } from '@layouts/header/header.component'
import { AboutMeComponent } from '@pages/about-me/about-me.component'
import { ExperienceComponent } from '@pages/experience/experience.component'
import ProjectsComponent from '@pages/projects/projects.component'
import { ServicesComponent } from '@pages/services/services.component'
import { DarkModeService } from '@services/dark-mode.service'

@Component({
  selector: 'app-home',
  standalone: true,
  imports: [
    RouterOutlet,
    HeaderComponent,
    HeroComponent,
    ExperienceComponent,
    ProjectsComponent,
    AboutMeComponent,
    ServicesComponent,
    FooterComponent,
    CodeIcon
  ],
  templateUrl: './home.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export default class HomeComponent {
  public darkModeService = inject(DarkModeService)
  public darkMode = computed(() => this.darkModeService.darkMode())
  constructor() {
    this.darkModeService.applyDarkMode()
  }
}

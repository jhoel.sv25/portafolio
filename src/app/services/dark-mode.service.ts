import { Injectable, signal } from '@angular/core'

@Injectable({
  providedIn: 'root'
})
export class DarkModeService {
  public darkMode = signal<boolean>(false)
  private matchMedia: MediaQueryList
  constructor() {
    this.matchMedia = window.matchMedia('(prefers-color-scheme: dark)')
  }

  public toggleDarkMode(): void {
    console.log('Dark mode applied', this.darkMode())
    this.darkMode.update((darkMode) => !darkMode)
  }

  public applyDarkMode(): string {
    if (localStorage.getItem('theme')) {
      return localStorage.getItem('theme') || 'system'
    }
    return window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light'
  }

  public getThemePreference(): string {
    if (localStorage.getItem('theme')) {
      return localStorage.getItem('theme') || 'system'
    }
    return window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light'
  }

  public updateThemePreference(theme: string): void {
    localStorage.setItem('theme', theme)
    this.applyDarkMode()
  }
}

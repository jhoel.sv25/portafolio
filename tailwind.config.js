
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/**/*.{html, ts}",
  ],
  darkMode:'class',
  theme: {
    extend: {
      container: {
        center: true,
        padding: '1rem',
        maxWidth: '1200px',
        screens: {
          'lg': '1200px',
        }
      },
      colors: {
        'lima': {
        '50': '#f1ffe5',
        '100': '#dfffc7',
        '200': '#c0ff96',
        '300': '#97fd59',
        '400': '#72f427',
        '500': '#4bca07',
        '600': '#3baf01',
        '700': '#2e8506',
        '800': '#28680c',
        '900': '#23580f',
        '950': '#0e3102',
    },
    
      }
    },
  },
  plugins: [
    require('@tailwindcss/typography'),
  ],
}

